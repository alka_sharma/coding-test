<?php
/**
* Template Name: Automobiles
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header();
?>
    <?php 
    $args = array(
        'post_type'   => 'automobiles',
        'post_status' => 'publish',
        'orderby' => 'title', 
        'order' => 'ASC',
    );
    $loop = new WP_Query( $args );
    ?>
    <?php  while ( $loop->have_posts() ) : $loop->the_post();   ?>
        <h2><a href="<?php the_permalink() ?>"><?php   print the_title(); ?></a></h2>
    <?php endwhile; ?>
    
<?php get_footer();?>